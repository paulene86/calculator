package com.paulo.poc.calculator.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.paulo.poc.calculator.payload.model.ExpressionBody;
import com.paulo.poc.calculator.service.impl.OperationsServiceImpl;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OperationsControllerTest {

    @InjectMocks
    OperationsController underTest;

    @Mock
    OperationsServiceImpl operationsService;

    @Test
    public void operationNumbersReturnOk() {
        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator("+");
        expressionBody.setOperands(Arrays.asList(new BigDecimal(12), new BigDecimal(23)));
        Mockito.when(operationsService.operationNumbers(expressionBody)).thenReturn(new BigDecimal(35));

        //when
        BigDecimal result = underTest.operationNumbers(expressionBody);
        //then
        assertEquals(new BigDecimal(35), result);
    }

    @Test
    public void operationNumbersReturnError() {
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(null);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(12),new BigDecimal(23)));
        Mockito.when(operationsService.operationNumbers(expressionBody)).thenThrow(NullPointerException.class);

        //then
        assertThrows(NullPointerException.class, () -> {
            underTest.operationNumbers(expressionBody);});

    }

}
