package com.paulo.poc.calculator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.paulo.poc.calculator.payload.model.ExpressionBody;
import com.paulo.poc.calculator.service.impl.OperationsServiceImpl;
import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OperationsServiceImplTest {

    private static final String ADD_OPERATOR = "+";
    private static final String SUBSTRACT_OPERATOR = "-";
    private static final String UNKNOW_OPERATOR = "*";

    @InjectMocks
    private OperationsServiceImpl underTest;

    @Test
    public void operationNumbersWithCorrectOperatorAddReturnOK(){
        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(ADD_OPERATOR);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(12),new BigDecimal(23)));
        //when
        BigDecimal result=underTest.operationNumbers(expressionBody);
        //then
        assertEquals(new BigDecimal(35), result);
    }

    @Test
    public void operationNumbersWithCorrectOperatorAddDecimalReturnOK () {
        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(ADD_OPERATOR);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(10.54),new BigDecimal(20)));

        //when
        BigDecimal result=underTest.operationNumbers(expressionBody);
        //then
        assertEquals(new BigDecimal(30.54).setScale(2,0), result.setScale(2,0));
    }

    @Test
    public void operationNumbersWithCorrectOperatorSubstractReturnOK () {
        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(SUBSTRACT_OPERATOR);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(1099),new BigDecimal(99.95)));

        //when
        BigDecimal result=underTest.operationNumbers(expressionBody);
        //then
        assertEquals(new BigDecimal(999.05).setScale(2,0), result.setScale(2,0));
    }

    @Test
    public void operationNumbersWithOneOperandReturnOK () {
        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(SUBSTRACT_OPERATOR);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(50)));

        //when
        BigDecimal result=underTest.operationNumbers(expressionBody);
        //then
        assertEquals(new BigDecimal(50), result);
    }

    @Test
    public void operationNumbersWithUnknowOperandReturnOK() {

        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(UNKNOW_OPERATOR);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(50)));
        //when
        BigDecimal result=underTest.operationNumbers(expressionBody);
        //then
        assertEquals(new BigDecimal(0), result);

    }

    @Test
    public void operationNuNumbersWithNoOperatorReturnException() {

        //given
        ExpressionBody expressionBody = new ExpressionBody();
        expressionBody.setOperator(null);
        expressionBody.setOperands(Arrays.asList(new BigDecimal(50)));

        assertThrows(NullPointerException.class, () -> {
            underTest.operationNumbers(expressionBody);});

    }







}
