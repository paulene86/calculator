package com.paulo.poc.calculator.controller;

import com.paulo.poc.calculator.payload.model.ExpressionBody;
import com.paulo.poc.calculator.service.OperationsService;
import java.math.BigDecimal;
import javax.ws.rs.POST;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/operations")
public class OperationsController {


    private final OperationsService operationsService;

    public OperationsController(OperationsService operationsService){
        this.operationsService = operationsService;
    }

    @POST
    @RequestMapping("/basic-operation")
        public BigDecimal operationNumbers(@RequestBody ExpressionBody inParameter){
        return operationsService.operationNumbers(inParameter);
    }



}
