package com.paulo.poc.calculator.payload.model;

import java.math.BigDecimal;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpressionBody {


    private String operator;
    private List<BigDecimal> operands;

}
