package com.paulo.poc.calculator.service;

import com.paulo.poc.calculator.payload.model.ExpressionBody;
import java.math.BigDecimal;

public interface OperationsService {

     BigDecimal operationNumbers(ExpressionBody expressionBody);

}
