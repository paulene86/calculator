package com.paulo.poc.calculator.service.impl;

import com.paulo.poc.calculator.payload.model.ExpressionBody;
import com.paulo.poc.calculator.service.OperationsService;
import io.corp.calculator.TracerImpl;
import java.math.BigDecimal;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class OperationsServiceImpl implements OperationsService {

    TracerImpl tracer = new TracerImpl();

    public   BigDecimal operationNumbers(ExpressionBody expressionBody) {

        return Optional.ofNullable(expressionBody.getOperator())
            .map(
                operator -> {
                    switch (operator) {
                        case "+":
                            return Optional.ofNullable(expressionBody.getOperands())
                                .map(operand-> operand.stream().reduce(BigDecimal.ZERO, BigDecimal::add))
                                .orElseThrow(() -> {
                                    tracer.trace("ERROR-->AT LEAST ONE OPERAND IS MANDATORY FOR ADDING");
                                    return new NullPointerException();});
                        case "-":
                            if (expressionBody.getOperands()==null || expressionBody.getOperands().isEmpty()){
                                tracer.trace("ERROR-->AT LEAST ONE OPERAND IS MANDATORY FOR SUBTRACTING");
                                throw new NullPointerException();
                            } else {
                            BigDecimal firstValue = expressionBody.getOperands().get(0);
                            return expressionBody.getOperands().size() <= 1
                                ? firstValue
                                : firstValue
                                    .subtract(
                                        expressionBody.getOperands().stream().skip(1)
                                            .reduce(BigDecimal.ZERO, BigDecimal::add));}
                        //case "others"
                        default:
                            return new BigDecimal(0);
                    }
                }
            )
            .orElseThrow(() -> {
                tracer.trace("ERROR-->OPERAND IS MANDATORY");
                return new NullPointerException();
            });
    }

}




