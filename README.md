#CALCULATOR API

##INTRODUCTION
    This is a basic calculator with adding and subtract operation
    You can call to the endpoint 
    POST 
        http://localhost:8080/operations/basic-operation
    With a scheme json as next :
    {"operator":"+","operands":[1,32,3]}'
    Operator can be "+" or "-" unknow operator return 0;
    The array the operands values can be from 1 to n , incorrect return internal server error;

##INSTALATION
    From the path where you have the project fo next:
    mvn install:install-file -Dfile=${PATH_TO_LOCAL_REPO}/tracer-1.0.0.jar -DgroupId=calculator    -DartifactId=tracer -Dversion=1.0.0
    mvn exec:java

##CURLS EXAMPLES
    1-

    curl --header "Content-Type: application/json"   --request POST   --data '{"operator":"+","operands":[1,32]}'   http://localhost:8080/operations/basic-operation

    2

    curl --header "Content-Type: application/json"   --request POST   --data '{"operator":"-","operands":[1,100]}'   http://localhost:8080/operations/basic-operation





